package ApiDanhMucLinhVucThuTucHanhChinh;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Result {
	
	@JsonProperty("MALINHVUC")
	private String maLinhVuc;
	
	@JsonProperty("TENLINHVUC")
	private String tenLinhVuc;
	
	@JsonProperty("MANGANH")
	private String maNganh;

	public String getMaLinhVuc() {
		return maLinhVuc;
	}

	public void setMaLinhVuc(String maLinhVuc) {
		this.maLinhVuc = maLinhVuc;
	}

	public String getTenLinhVuc() {
		return tenLinhVuc;
	}

	public void setTenLinhVuc(String tenLinhVuc) {
		this.tenLinhVuc = tenLinhVuc;
	}

	public String getMaNganh() {
		return maNganh;
	}

	public void setMaNganh(String maNganh) {
		this.maNganh = maNganh;
	}

}
