package ApiDanhMucLePhi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Result {
	
	@JsonProperty("MAPHILEPHI")
	private String maPhiLePhi;
	
	@JsonProperty("TENPHILEPHI")
	private String tenPhiLePhi;

	public String getMaPhiLePhi() {
		return maPhiLePhi;
	}

	public void setMaPhiLePhi(String maPhiLePhi) {
		this.maPhiLePhi = maPhiLePhi;
	}

	public String getTenPhiLePhi() {
		return tenPhiLePhi;
	}

	public void setTenPhiLePhi(String tenPhiLePhi) {
		this.tenPhiLePhi = tenPhiLePhi;
	}
}
