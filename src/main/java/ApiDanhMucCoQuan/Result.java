package ApiDanhMucCoQuan;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Result {
	
	@JsonProperty("MADONVI")
	private String maDonVi;
	
	@JsonProperty("TENDONVI")
	private String tenDonVi;
	
	@JsonProperty("CAPDONVI")
	private int capDonVi;

	public String getMaDonVi() {
		return maDonVi;
	}

	public void setMaDonVi(String maDonVi) {
		this.maDonVi = maDonVi;
	}

	public String getTenDonVi() {
		return tenDonVi;
	}

	public void setTenDonVi(String tenDonVi) {
		this.tenDonVi = tenDonVi;
	}

	public int getCapDonVi() {
		return capDonVi;
	}

	public void setCapDonVi(int capDonVi) {
		this.capDonVi = capDonVi;
	}
}
