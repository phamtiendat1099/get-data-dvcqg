package ApiDanhMucTrangThaiHoSo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Result {
	
	@JsonProperty("TRANGTHAIHOSO")
	private String trangThaiHoSo;
	
	@JsonProperty("TENTRANGTHAI")
	private String tenTrangThai;

	public String getTrangThaiHoSo() {
		return trangThaiHoSo;
	}

	public void setTrangThaiHoSo(String trangThaiHoSo) {
		this.trangThaiHoSo = trangThaiHoSo;
	}

	public String getTenTrangThai() {
		return tenTrangThai;
	}

	public void setTenTrangThai(String tenTrangThai) {
		this.tenTrangThai = tenTrangThai;
	}
}
