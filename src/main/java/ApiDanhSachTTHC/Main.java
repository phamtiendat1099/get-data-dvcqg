package ApiDanhSachTTHC;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.List;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.example.demo.Convert;

public class Main {
	
	public static void main(String arg[]) throws IOException {
		Body result = sendPOST("http://14.225.12.119:80/VXPAdapter/RestService/forward/mapi/call");
        insert(result.getResult());
	}
	
	 private static Body sendPOST(String url) throws IOException{
		 Body result = null;
		 HttpPost post = new HttpPost(url);
	     post.addHeader("content-type", "application/json");
	     post.addHeader("charset", "utf-8");

	     StringBuilder json = new StringBuilder();
	     json.append("{");
	     json.append("\"session\":\"FAWM35+FqSaDcFpHYV47bgSh/CSuOUu7AQY+H4kvXRu3rNVafGA32iUaOQ190pe7sixmSYphKlri3/NOLnsumV7t50XxOl22\",");
	     json.append("\"madonvi\":\"01.01.H02\",");
	     json.append("\"service\":\"LayDanhSachTTHC\"");
	     json.append("}");
	     

	     // send a JSON data
	     post.setEntity(new StringEntity(json.toString()));

	     try (CloseableHttpClient httpClient = HttpClients.createDefault();
	          CloseableHttpResponse response = httpClient.execute(post)) {
	        	
	          result = Convert.textToObject(EntityUtils.toString(response.getEntity()), Body.class);
	            
	     }catch (IOException e) {
	         e.printStackTrace();
	     }

	     return result;
	 }
	 
	 public static void insert(List<Result> a) {
		 try
		    {
		      // create a mysql database connection
			 String userName = "vdxp";
			 String password = "Vdxp@123";
		      
			 String connectionURL = "jdbc:mysql://103.127.207.181:3306/db_vdxp?useUnicode=yes&characterEncoding=UTF-8";
			 
			 DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
			 
		        // 2. Open connection
		     Connection con = DriverManager.getConnection(connectionURL, userName, password);
		 
		        // 3. Create Statement
		     Statement st = con.createStatement();
		     
		     for(int i = 0; i<a.size();i++){
		    	 String x = a.get(i).getTenTTHC();
		    	 x = x.replace('\'',' ');

		    	 String query = " insert into thu_tuc_hanh_chinh(ma_tthc,ten_tthc,ma_co_quan_cong_bo)" + " values ('"+a.get(i).getMaTTHC()
		    			 + "','"+x
		    			 +"','"+a.get(i).getMaCoQuanCongBo()+"')";
		    	 st.addBatch(query);
					
				//execute and commit batch of 1000 queries
				if(i%1000 ==0) {
					st.executeBatch();
					System.out.println("Load  Api Danh Sach TTHC: "+(i+1)+"/"+a.size());
				}
			 }
		     System.out.println("QUERY DONE:");
		     st.executeBatch();
		     con.close();
		     st.close();
		    }
		    catch (Exception e)
		    {
		      System.err.println("Got an exception!");
		      System.err.println(e.getMessage());
		    }
	 }

}
