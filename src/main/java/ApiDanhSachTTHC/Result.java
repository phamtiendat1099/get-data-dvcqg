package ApiDanhSachTTHC;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Result {
	
	@JsonProperty("MATTHC")
	private String maTTHC;
	
	@JsonProperty("TENTTHC")
	private String tenTTHC;
	
	@JsonProperty("MACOQUANCONGBO")
	private String maCoQuanCongBo;

	public String getMaTTHC() {
		return maTTHC;
	}

	public void setMaTTHC(String maTTHC) {
		this.maTTHC = maTTHC;
	}

	public String getTenTTHC() {
		return tenTTHC;
	}

	public void setTenTTHC(String tenTTHC) {
		this.tenTTHC = tenTTHC;
	}

	public String getMaCoQuanCongBo() {
		return maCoQuanCongBo;
	}

	public void setMaCoQuanCongBo(String maCoQuanCongBo) {
		this.maCoQuanCongBo = maCoQuanCongBo;
	}
}
