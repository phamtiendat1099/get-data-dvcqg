package com.example.demo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PhuongXa {

	@JsonProperty("MAPHUONGXA")
	private String maPhuongXa;
	
	@JsonProperty("TENPHUONGXA")
	private String tenPhuongXa;
	
	@JsonProperty("MAQUANHUYEN")
	private String maQuanHuyen;

	public String getMaPhuongXa() {
		return maPhuongXa;
	}

	public void setMaPhuongXa(String maPhuongXa) {
		this.maPhuongXa = maPhuongXa;
	}
	

	public String getTenPhuongXa() {
		return tenPhuongXa;
	}

	public void setTenPhuongXa(String tenPhuongXa) {
		this.tenPhuongXa = tenPhuongXa;
	}

	public String getMaQuanHuyen() {
		return maQuanHuyen;
	}

	public void setMaQuanHuyen(String maQuanHuyen) {
		this.maQuanHuyen = maQuanHuyen;
	}

}
