package com.example.demo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QuanHuyen {

	@JsonProperty("MAQUANHUYEN")
	private String maquanhuyen;
	
	@JsonProperty("TENQUANHUYEN")
	private String tenquanhuyen;
	
	@JsonProperty("MATINHTHANH")
	private String matinhthanh;

	public String getMaquanhuyen() {
		return maquanhuyen;
	}

	public void setMaquanhuyen(String maquanhuyen) {
		this.maquanhuyen = maquanhuyen;
	}

	public String getTenquanhuyen() {
		return tenquanhuyen;
	}

	public void setTenquanhuyen(String tenquanhuyen) {
		this.tenquanhuyen = tenquanhuyen;
	}

	public String getMatinhthanh() {
		return matinhthanh;
	}

	public void setMatinhthanh(String matinhthanh) {
		this.matinhthanh = matinhthanh;
	}
	
}
