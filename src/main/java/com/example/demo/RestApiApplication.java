package com.example.demo;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.sql.*;

@SpringBootApplication
public class RestApiApplication {

	public static void main(String[] args) {
		//SpringApplication.run(RestApiApplication.class, args);
		
		try {
            Ok3 result = sendPOST("http://14.225.12.119:80/VXPAdapter/RestService/forward/mapi/call");
            System.out.println("Pham Tien Dat"+result.getResult().size());
            insert(result.getResult());
        } catch (IOException e) {
            e.printStackTrace();
        }

	}
	
	 private static Ok3 sendPOST(String url) throws IOException {

	        Ok3 result = null;
	        HttpPost post = new HttpPost(url);
	        post.addHeader("content-type", "application/json");
	        post.addHeader("charset", "utf-8");

	        StringBuilder json = new StringBuilder();
	        json.append("{");
	        json.append("\"session\":\"Ye0LqEK4cVJYP4SDMj5urkqdCHJVRgFBQnFC2ZPXaPl56dQW6rYBE9SEf6pEPcsb8unz9JnyhgXbxmS8kmdmKoy6IqH+D1tX\",");
	        json.append("\"madonvi\":\"000.00.00.H26\",");
	        json.append("\"service\":\"LayDanhMucPhuongXa\"");
	        json.append("}");

	        // send a JSON data
	        post.setEntity(new StringEntity(json.toString()));

	        try (CloseableHttpClient httpClient = HttpClients.createDefault();
	             CloseableHttpResponse response = httpClient.execute(post)) {
	        	
	            result = Convert.textToObject(EntityUtils.toString(response.getEntity()), Ok3.class);
	            
	        }catch (IOException e) {
	            e.printStackTrace();
	        }

	        return result;
	    }
	 
	 public static void insert(List<PhuongXa> a) {
		 try
		    {
		      // create a mysql database connection
			 String userName = "vdxp";
			 String password = "Vdxp@123";
		      
			 String connectionURL = "jdbc:mysql://103.127.207.181:3306/db_vdxp?useUnicode=yes&characterEncoding=UTF-8";
			 
			 DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
			 
		        // 2. Open connection
		     Connection con = DriverManager.getConnection(connectionURL, userName, password);
		 
		        // 3. Create Statement
		     Statement st = con.createStatement();
 
		     for(int i = 0; i<a.size();i++){
		    	 String x = a.get(i).getTenPhuongXa();
		    	 x = x.replace('\'',' ');

		    	 String query = " insert into phuong_xa" + " values ('"+a.get(i).getMaPhuongXa()
		    			 + "','"+x
		    			 + "','"+a.get(i).getMaQuanHuyen()+"' )";
		    	 st.executeUpdate(query);
			      
			 }
		     
		     con.close();
		     st.close();
		    }
		    catch (Exception e)
		    {
		      System.err.println("Got an exception!");
		      System.err.println(e.getMessage());
		    }
	 }

}
