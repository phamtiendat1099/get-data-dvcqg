package com.example.demo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Ok3 {
	

	private List<PhuongXa>result;

	private String error_code ;

	public List<PhuongXa> getResult() {
		return result;
	}

	public void setResult(List<PhuongXa> result) {
		this.result = result;
	}

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}
	
	
}
