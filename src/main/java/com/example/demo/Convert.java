package com.example.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Convert {
	
	public static <T> T textToObject(String text, Class<T> classObject)
			throws JsonMappingException, JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		T result = objectMapper.readValue(text, classObject);
		return result;
	}
}
