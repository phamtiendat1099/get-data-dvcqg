package com.example.demo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Tinh {
	
	@JsonProperty("MATINHTHANH")
	private String matinhthanh;
	
	@JsonProperty("TENTINHTHANH")
	private String tentinhthan;
	
	public Tinh() {
	}

	public String getMatinhthanh() {
		return matinhthanh;
	}

	public void setMatinhthanh(String matinhthanh) {
		this.matinhthanh = matinhthanh;
	}

	public String getTentinhthan() {
		return tentinhthan;
	}

	public void setTentinhthan(String tentinhthan) {
		this.tentinhthan = tentinhthan;
	}
	
	
}
